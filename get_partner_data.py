#!/usr/bin/env python

import sh
import sys
import logging

import gspread
import pandas as pd

from csv import QUOTE_NONNUMERIC
from argparse import ArgumentParser

from oauth2client.service_account import ServiceAccountCredentials

# ids for BigQuery ga_sessions_ tables
OMG_ID="95387438"
DOSE_ID="95246840"

# query for device grouping
ALL_BY_DEVICE_QUERY = """
select
  date as datestring
  ,concat(ifnull(lower(trafficSource.source),"_"),
          "#",
          ifnull(lower(trafficSource.medium),"_"),
          "#",
          ifnull(lower(trafficSource.campaign),"_")) src_med_cmp
  ,device.deviceCategory as device
  -- ,hits.eventInfo.eventCategory
  ,sum(totals.visits) as sessions
  ,sum(totals.pageviews) as pgviews
from
  table_date_range([{site_id}.ga_sessions_],
                   timestamp('{start_date_str}'),
                   timestamp('{end_date_str}'))
{where_clause}
group by
  device, src_med_cmp, datestring
"""

# query for location grouping
ALL_BY_LOCATION_QUERY = """
select
  date as datestring
  ,concat(ifnull(lower(trafficSource.source),"_"),
          "#",
          ifnull(lower(trafficSource.medium),"_"),
          "#",
          ifnull(lower(trafficSource.campaign),"_")) src_med_cmp
  ,case
      when geoNetwork.country not in ("Australia", "Canada", "United Kingdom", "United States") then 'Other'
      else geoNetwork.country
   end as country
  ,sum(totals.visits) as sessions
  ,sum(totals.pageviews) as pgviews
from
  table_date_range([{site_id}.ga_sessions_],
                   timestamp('{start_date_str}'),
                   timestamp('{end_date_str}'))
{where_clause}
group by
  country, src_med_cmp, datestring
"""

# query dictionary
QUERY_DICT = {
    'device': ALL_BY_DEVICE_QUERY,
    'country': ALL_BY_LOCATION_QUERY,
}


def get_bigquery_results(raw_query, site_id, where_clause,
                         start_date_str, end_date_str, outfile_name,
                         keep=False):
    """
    use sh to call google BigQuery utility 'bq' to execute parameterized query
    and return results dataframe from returned csv file
    """

    query = raw_query.format(site_id=site_id, where_clause=where_clause,
                             start_date_str=start_date_str, end_date_str=end_date_str)
    logging.info(query)

    sh.bq("--format=csv", "query", "-q", "-n", "10000000", _in=query, _out=outfile_name)
    df = pd.read_csv(outfile_name, parse_dates=['datestring'])
    if (not keep):
        sh.rm(outfile_name)
    return df


def add_partner(df, prtnr_df):
    """
    add the Partner field to the supplied df based on source/medium/campaign
    info from prtnr_df
    """

    # create blank field
    df['Partner'] = ""

    # do assignments from prtnr_df
    cols = ['Partner', 'Source', 'Medium', 'Campaign']
    for t in prtnr_df[cols].itertuples():
        rows = (df.source == t.Source.lower())

        if (type(t.Medium)==str and len(t.Medium)>0):
            rows &= (df.medium == t.Medium.lower())

        if (type(t.Campaign)==str and len(t.Campaign)>0):
            rows &= (df.campaign == t.Campaign.lower())

        if (sum(rows) > 0):
            logging.info("%5d %s" % (sum(rows), t))
            df.loc[rows, 'Partner'] = t.Partner

    # handle special cases
    rows = ((df.source == 'partners') & (df.medium == 'aol'))
    df.loc[rows, 'Partner'] = 'AOL'

    rows = (df.campaign.str.contains("cmfacebook"))
    df.loc[rows, 'Partner'] = 'Cybrid Media'

    rows = ((df.campaign == 'dyk') | (df.medium == 'dyk'))
    df.loc[rows, 'Partner'] = 'Everhance'

    rows = ((df.source=='facebook') & (df.medium=='ads') & (~df.campaign.str.contains('saba')))
    df.loc[rows, 'Partner'] = 'Facebook Ads'

    rows = ((df.source=='facebook') & (df.medium=='social'))
    df.loc[rows, 'Partner'] = 'Facebook Owned'

    rows = (df.source=='ism')
    df.loc[rows, 'Partner'] = 'Inner Splendor Media'

    rows = (df.source=='sz-social')
    df.loc[rows, 'Partner'] = 'Liquid Social'

    rows = (df.medium=='pages')
    df.loc[rows, 'Partner'] = 'Lolspots'

    rows = ((df.src_med_cmp.str.contains("twitter#omgfacts")) |
            (df.src_med_cmp.str.contains("omgfacts#twitter")))
    df.loc[rows, 'Partner'] = 'Twitter Owned'

    rows = (df.medium=='whfacebook')
    df.loc[rows, 'Partner'] = 'Wild Hair'

    rows = (df.Partner == "")
    df.loc[rows, 'Partner'] = 'Other'

    return df


def set_pandas_options():
    """
    set some pandas display options
    """
    pd.set_option('display.width', 500)
    pd.set_option('display.precision', 5)
    pd.set_option('display.max_rows', 5000)
    pd.set_option('display.max_colwidth', 100)
    pd.set_option('display.max_columns', 1000)
    pd.set_option('display.expand_frame_repr', False)


def config_logging(level):
    """
    configure logging level
    """

    LEVELS = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }
    if (level is not None):
        fmt = "%(asctime)s - %(levelname)s - %(message)s"
        logging.basicConfig(format=fmt,
                            level=LEVELS.get(level, logging.NOTSET))


def parse_args():
    """
    parse command line arguments
    """
    
    desc = "BigQuery Partner Reports"
    parser = ArgumentParser(description=desc)

    parser.add_argument('-q', dest='query_type', default="device")
    parser.add_argument('-L', dest='log_level', default=None)
    parser.add_argument('-S', dest='start_date', default=None)
    parser.add_argument('-E', dest='end_date', default=None)
    parser.add_argument('-t', dest='traffic_partner_file', default=None)
    parser.add_argument('-T', dest='traffic_partner_sheet', default="Traffic Partners")
    parser.add_argument('-s', dest='do_shares', action='store_true')

    return parser.parse_args()


def read_partners_from_sheet(sheet_name):
    """
    read traffic partner data from specified Google Sheet
    """

    # authorize
    scope = ['https://spreadsheets.google.com/feeds']
    filename = 'dose-sheet-reader-38d4ab47f54a.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(filename, scope)
    gc = gspread.authorize(credentials)

    # grab sheet data
    wks = gc.open(sheet_name)
    sheet = wks.worksheets()[0]
    records = sheet.get_all_records()

    # create DF to return from records
    partner_df = pd.DataFrame(records)

    return partner_df


def main():
    "main function"

    set_pandas_options()

    args = parse_args()
    config_logging(args.log_level)
    logging.debug("Args: %s" % args)

    query = QUERY_DICT.get(args.query_type)
    if (query is None):
        logging.error("No such query type: '%s'" % args.query_type)
        sys.exit(1)

    where_clause = ""
    if (args.do_shares):
        where_clause = 'where hits.eventInfo.eventCategory = "Share"\n'

    logging.info("Grabbing OMG data")
    bq_omg_df = get_bigquery_results(query, OMG_ID, where_clause,
                                     args.start_date, args.end_date,
                                     "/tmp/bq.%s.csv" % OMG_ID)
    bq_omg_df['site'] = 'OMG'

    logging.info("Grabbing Dose data")
    bq_dose_df = get_bigquery_results(query, DOSE_ID, where_clause,
                                      args.start_date, args.end_date,
                                      "/tmp/bq.%s.csv" % DOSE_ID)
    bq_dose_df['site'] = 'DOSE'


    # assemble both sites into one DF
    bq_df = pd.concat([bq_omg_df, bq_dose_df])
    values = ['src_med_cmp', args.query_type, 'datestring']
    bq_df = bq_df.sort_values(values).reset_index(drop=True)

    # split out source, medium, campaign from concatenated field
    splitter = lambda x: pd.Series(x.split("#"), index=['source','medium','campaign'])
    joined = bq_df.join(bq_df.src_med_cmp.apply(splitter))

    # read partner info
    if (args.traffic_partner_file is not None):
        partner_df = pd.read_csv(args.traffic_partner_file)
    else:
        partner_df = read_partners_from_sheet(args.traffic_partner_sheet)

    joined_prtnr = add_partner(joined, partner_df)

    cols = ['Partner']
    if (not args.do_shares): cols.append(args.query_type)
    totals = joined_prtnr.groupby(cols, as_index=False).sum()
    totals = totals.rename(columns={'pgviews': 'Pageviews',
                                    'sessions': 'Sessions'})

    pivoted = totals.pivot(index='Partner', columns=args.query_type)
    pivoted = pivoted.fillna(0.0).astype(int)

    pivoted['Sessions', 'Total'] = pivoted.Sessions.sum(axis=1)
    pivoted['Pageviews','Total'] = pivoted.Pageviews.sum(axis=1)

    types = ['Sessions', 'Pageviews']
    if (args.query_type == 'country'):
        categories = ['Australia', 'Canada', 'Other', 'United Kingdom', 'United States', 'Total']
    else:
        categories = ['desktop', 'mobile', 'tablet', 'Total']

    mi_tuples = [(t,c) for t in types for c in categories]
    multi_index = pd.MultiIndex.from_tuples(mi_tuples)
    pivoted = pd.DataFrame(pivoted, columns=multi_index)

    print pivoted.to_csv(quoting=QUOTE_NONNUMERIC)


if (__name__ == "__main__"):
    main()


